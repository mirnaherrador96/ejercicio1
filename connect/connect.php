<?php
    class connect
    {
        //DEFINE LOS ELEMENTOS DE CONEXION
        private static $host = "localhost";
        private static $dbname = "db_auto";
        private static $user = "herrador";
        private static $pass = "admin";
        protected $pdo = null;
        //FUNCION PARA CONECTAR A LA BASE DE DATOS
        private function opendb()
        {
            $this ->pdo = new PDO("mysql:host=".self::$host.";dbname=".self::$dbname,self::$user,self::$pass);
            $this ->pdo ->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        }
        //FUNCION PARA CONSULTAS DE TIPO SELECT
        public function query($sql='',$arraydata= array()){
            try {
                if ($this ->pdo == null) {
                    $this ->opendb();
                }
                $st = $this ->pdo->prepare($sql);
                $st->execute($arraydata);
                return $st;
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        }
        public function query_select($sql='',$arraydata= array()){
            try {
                if ($this ->pdo == null) {
                    $this ->opendb();
                }
                $st = $this ->pdo->prepare($sql);
                $st->execute($arraydata);
                $rows = $st->fetchAll(PDO::FETCH_ASSOC);
                return $rows;
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        }
    }
?>
