<?php
include_once("../connect/connect.php");
$pdo = new connect();
$id = $_GET['codigo'];
if (isset($_POST["guardar"])) {
    $idMarca          =      $_POST["idMarca"];
    $nombre           =      $_POST["nombre"];
    $descripcion      =      $_POST["descripcion"];
    $tipoCombustible  =      $_POST["tipoCombustible"];
    $cantidadPuertas  =      $_POST["cantidadPuertas"];
    $precio           =      $_POST["precio"];

    $sql = "UPDATE autos set  idMarca =:idMarca, nombre =:nombre, descripcion =:descripcion, tipoCombustible =:tipoCombustible, cantidadPuertas =:cantidadPuertas, precio =:precio where idAutos =  ". $id;
    $datos = array(
        ':idMarca'          =>  $idMarca,
        ':nombre'           =>  $nombre,
        ':descripcion'      =>  $descripcion,
        ':tipoCombustible'  =>  $tipoCombustible,
        ':cantidadPuertas'  =>  $cantidadPuertas,
        ':precio'           =>  $precio
    );
    $pdo->query($sql, $datos);

    $sql = "INSERT INTO registrosDB (idRegistrosDB, accion, fechaCreacion) VALUES 
                (null, :accion, now())";
    $accion = "Se agrego un auto";
    $datos = array(
        ':accion'    =>  $accion
    );
    $pdo->query($sql, $datos);

    echo "<div class=\"alert alert-success text-center\" role=\"alert\">";
    echo "Se ha guardado el registro";
    echo "</div>";
} else {
    echo "<div class=\"alert alert-danger text-center\" role=\"alert\">";
    echo "No se ha guardado registro. ";
    echo "</div>";
}
$sql = "SELECT * FROM autos WHERE idAutos= :id";
$valor = array(':id'    => $id);
$datos = $pdo->query_select($sql,$valor);
$row = $datos[0];
?>
<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" />
    <title>Editar auto</title>
</head>

<body>
    <br />
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h4>Editar auto</h4>
            </div>
            <div class="card-body">
                <form action="" method="POST">
                    <div class="form-row">

                        <div class="form-group col-md-6">
                            <label for="idMarca">Marca</label>
                            <select class="form-control" name="idMarca" id="idMarca" required>
                                <option value=""></option>
                                <?php
                                $sql = "SELECT * FROM marcas";
                                $datos = $pdo->query_select($sql);
                                foreach ($datos as $key => $value) {
                                    if ($row['idMarca'] == $value['idMarca']) {
                                        echo "<option value='" . $value['idMarca'] . "' selected=''>" . $value['nombreMarca'] . "</option>";
                                    } else {
                                        echo "<option value='" . $value['idMarca'] . "'>" . $value['nombreMarca'] . "</option>";
                                    }
                                    
                                    
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="nombre">Nombre:</label>
                            <input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo $row['nombre']; ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="descripcion">Descripción</label>
                            <textarea class="form-control" name="descripcion" id="descripcion" rows="2" required><?php echo $row['descripcion']; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tipoCombustible">Combustible</label>
                            <select class="form-control" name="tipoCombustible" id="tipoCombustible" required>
                                <option value=""><?php echo $row['tipoCombustible']; ?></option>
                                <option value="Diesel">Diesel</option>
                                <option value="Gasolina">Gasolina</option>
                                <option value="GasLicuado">GasLicuado</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="cantidadPuertas">Cantidad de puertas:</label>
                            <input type="number" class="form-control" name="cantidadPuertas" id="cantidadPuertas" value="<?php echo $row['cantidadPuertas']; ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="precio">Precio:</label>
                            <input type="text" class="form-control" name="precio" id="precio" value="<?php echo $row['precio']; ?>" required>
                        </div>
                        
                        
                        <div class="form-group col-md-6">
                            <input type="Submit" value="Guardar" name="guardar" class="btn btn-success float-right" />
                        </div>
                        <div class="form-group col-md-6">
                            <a class="btn btn-info" href="verAutos.php">Regresar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>
