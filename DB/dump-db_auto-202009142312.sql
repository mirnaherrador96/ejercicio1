-- MySQL dump 10.17  Distrib 10.3.23-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: db_auto
-- ------------------------------------------------------
-- Server version	10.3.23-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autos`
--

DROP TABLE IF EXISTS `autos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autos` (
  `idAutos` int(11) NOT NULL AUTO_INCREMENT,
  `idMarca` int(11) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descripcion` varchar(300) DEFAULT NULL,
  `tipoCombustible` varchar(50) DEFAULT NULL,
  `cantidadPuertas` int(11) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  PRIMARY KEY (`idAutos`),
  KEY `autos_idMarca_IDX` (`idMarca`) USING BTREE,
  CONSTRAINT `autos_FK` FOREIGN KEY (`idMarca`) REFERENCES `marcas` (`idMarca`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autos`
--

LOCK TABLES `autos` WRITE;
/*!40000 ALTER TABLE `autos` DISABLE KEYS */;
INSERT INTO `autos` VALUES (1,1,'jbnjnkj','jnjn','jmnbjmn',2,5),(2,1,'jbnjnkj','jnjn','jmnbjmn',2,5),(7,1,'jbnjnkj','jnjn','jmnbjmn',2,5),(8,1,'jbnjnkj','jnjn','jmnbjmn',2,5),(9,2,'mm','mm','mm',1,7),(10,2,'mm','mm','mm',1,7),(11,2,'mm','mm','mm',1,7),(12,2,'mm','mm','mm',1,7);
/*!40000 ALTER TABLE `autos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marcas`
--

DROP TABLE IF EXISTS `marcas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marcas` (
  `idMarca` int(11) NOT NULL AUTO_INCREMENT,
  `nombreMarca` varchar(100) DEFAULT NULL,
  `descripcionMarca` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idMarca`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marcas`
--

LOCK TABLES `marcas` WRITE;
/*!40000 ALTER TABLE `marcas` DISABLE KEYS */;
INSERT INTO `marcas` VALUES (1,'Subaru','mmmmmmmmm'),(2,'BMW','mmmmmmmmmm'),(3,'Mazda','mmmmmmmmmm'),(4,'Toyota','mmmmmmmmmm'),(5,'Kia','mmmmmmmmmm'),(6,'Honda','mmmmmmmmmm'),(7,'Hyundai','mmmmmmmmmm'),(8,'Volvo','mmmmmmmmmm'),(9,'Mercedes-Benz','mmmmmmmmmm'),(10,'Volkswagen','mmmmmmmmmm'),(11,'Scion','mmmmmmmmmm'),(12,'Ford','mmmmmmmmmm'),(13,'Acura','mmmmmmmmmm'),(14,'Chevrolet','mmmmmmmmmm'),(15,'Nissan','mmmmmmmmmm'),(16,'Mitsubishi','mmmmmmmmmm'),(17,'Jeep','mmmmmmmmmm'),(18,'Dodge','mmmmmmmmmm');
/*!40000 ALTER TABLE `marcas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registrosDB`
--

DROP TABLE IF EXISTS `registrosDB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registrosDB` (
  `idRegistrosDB` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(100) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  PRIMARY KEY (`idRegistrosDB`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registrosDB`
--

LOCK TABLES `registrosDB` WRITE;
/*!40000 ALTER TABLE `registrosDB` DISABLE KEYS */;
INSERT INTO `registrosDB` VALUES (1,'Se elimino un auto','2020-09-14 22:37:52'),(2,'Se elimino un auto','2020-09-14 22:37:56'),(3,'Se elimino un auto','2020-09-14 22:37:57'),(4,'Se elimino un auto','2020-09-14 22:37:58'),(5,'Se elimino un auto','2020-09-14 22:37:58'),(6,'Se elimino un auto','2020-09-14 22:37:59'),(7,'Se elimino un auto','2020-09-14 22:37:59'),(8,'Se elimino un auto','2020-09-14 22:37:59'),(9,'Se elimino un auto','2020-09-14 22:38:00'),(10,'Se elimino un auto','2020-09-14 22:38:57'),(11,'Se elimino un auto','2020-09-14 22:38:58'),(12,'Se elimino un auto','2020-09-14 22:38:59'),(13,'Se elimino un auto','2020-09-14 22:38:59');
/*!40000 ALTER TABLE `registrosDB` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'db_auto'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-14 23:12:16
