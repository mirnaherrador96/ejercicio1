<?php
include_once("../connect/connect.php");
$pdo = new connect();
    if(isset($_POST['buscar'])){
        $bus = $_POST['nombreLib'];
        $sql = "SELECT autos.*, marcas.nombreMarca FROM 
            autos, marcas WHERE autos.idMarca=marcas.idMarca and autos.nombre like '%".$bus."%'";
        $datos = $pdo->query($sql);

        $sql = "INSERT INTO registrosDB (idRegistrosDB, accion, fechaCreacion) VALUES 
                (null, :accion, now())";
        $accion = "Se busco un auto";
        $datos = array(
            ':accion'    =>  $accion
        );
        $pdo->query($sql, $datos);
    }else{
        $sql = "SELECT autos.*, marcas.nombreMarca FROM 
            autos, marcas WHERE autos.idMarca=marcas.idMarca";
        $datos = $pdo->query($sql);
    }
?>
<!doctype html>
<html lang="en">

<head>
    <title>Libros</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" />
</head>

<body>
    <br />
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-4">
                        <h4>Lista de autos</h4>
                    </div>
                    <div class="col-lg-6">
                        <form action="" method="POST">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <input type="text" placeholder="Buscar" class="form-control" name="nombreLib" id="nombreLib">
                                </div>
                                <div class="form-group col-md-6">
                                    <button class="btn btn-info" type="submit" name="buscar">
                                        <i class="fas fa-search"></i>
                                    </button >
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-2">
                        <a class="btn btn-success" href="agregarAuto.php"><i class="fas fa-plus"></i> Agregar</a>
                    </div>
                </div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Marca</th>
                        <th>Descripcion</th>
                        <th>Tipo Comb.</th>
                        <th>Cantidad puertas</th>
                        <th>Precio</th>
                        <th colspan="2">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($datos as $key => $value) {
                        echo "<tr>";
                        echo "<td>" . $value['nombre'] . "</td>";
                        echo "<td>" . $value['nombreMarca'] . "</td>";
                        echo "<td>" . $value['descripcion'] . "</td>";
                        echo "<td>" . $value['tipoCombustible'] . "</td>";
                        echo "<td>" . $value['cantidadPuertas'] . "</td>";
                        echo "<td>" . $value['precio'] . "</td>";
                        echo "<td colspan='2'>";
                        echo "<a class=\"btn btn-warning\" href=\"editarAuto.php?codigo=" . $value["idAutos"] . "\"><i class=\"fas fa-edit\"></i></a> ";
                        echo "<a class=\"btn btn-danger\" href=\"eliminarAuto.php?codigo=" . $value["idAutos"] . "\"><i class=\"far fa-trash-alt\"></i></a>";
                        echo "</td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>